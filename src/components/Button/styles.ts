import styled, { css } from 'styled-components';
import { shade } from 'polished';

interface ContainerProps {
  background?: string;
}
//
export const Container = styled.button<ContainerProps>`
  background: var(--color-primary);
  height: 5.6rem;
  border-radius: 1rem;
  border: 0;
  padding: 0 1rem;
  color: var(--color-button-text);
  width: 100%;
  font-weight: 500;
  margin-top: 2rem;
  transition: background-color 0.2s;

  &:hover {
    background: ${shade(0.5, '#001A4E')};
  }

  ${props =>
    props.background &&
    css`
      background: ${props.background};

      &:hover {
        background: ${shade(0.1, props.background)};
      }
    `};
`;
