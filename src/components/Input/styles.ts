import styled, { css } from 'styled-components';

import Tooltip from '../Tooltip';

interface ContainerProps {
  isFocused: boolean;
  isFilled: boolean;
  isErrored: boolean;
}

export const Container = styled.div<ContainerProps>`
  background: var(--color-input-background);
  border-radius: 1rem;
  border: 2px solid var(--color-input-background);
  padding: 1.5rem;
  width: 100%;
  color: #666360;
  display: flex;
  align-items: center;

  margin-bottom: 1rem;

  & + div {
    margin-top: 1rem;
  }

  ${props =>
    props.isErrored &&
    css`
      border-color: var(--color-error);
    `}

  ${props =>
    props.isFocused &&
    css`
      color: var(--color-secundary);
      border-color: var(--color-secundary);
    `}

  ${props =>
    props.isFilled &&
    css`
      color: var(--color-secundary);
    `}

  input {
    color: var(--color-input-text);
    flex: 1;
    max-width: calc(100% - 66px);
    background: transparent;
    border: 0;

    &::placeholder {
      color: #666360;
    }
  }

  svg {
    min-width: 20px;
    margin-right: 1rem;
  }
`;

export const Error = styled(Tooltip)`
  height: 20px;
  margin-left: 16px;

  svg {
    margin: 0;
  }

  span {
    background: var(--color-error);
    color: var(--color-title-in-primary);

    &::before {
      border-color: var(--color-error) transparent;
    }
  }
`;
