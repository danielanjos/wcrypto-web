import styled, { createGlobalStyle } from 'styled-components';

export default createGlobalStyle`
  :root {
    --color-background: #F3F5FB;

    --color-primary: #00134E;
    --color-secundary: #FF7600;

    --color-thead-background: #F6F9FC;

    --color-title-in-primary: #F2F5FF;
    --color-text-title: #76787B;
    --color-text-base: #353535;

    --color-input-background: #FFFFFF;
    --color-input-text: #464646;
    --color-box-base: #FFFFFF;

    --color-button-primary: #0000FF;
    --color-button-secundary: #FF7600;
    --color-button-text: #FFFFFF;

    --color-button-cancel: #7F7F7F;
    --color-error: #D9001B;

    --border: 2px ridge #a0a0a0;

    font-size: 60%;
  }

  *{
    margin: 0;
    padding: 0;
    box-sizing: border-box;
    outline: 0;
  }

  html, body, #root{
    height: 100vh;
    background: var(--color-background);
  }

  body{
    /* background: var(---color-background); */
    color: var(---color-text-base);
    -webkit-font-smoothing: antialiased;
  }

  body, input, button, textarea, select, option{
    font: 500 1.6rem Roboto;
  }

  h1, h2, h3, h4, h5, h6, strong{
    font-weight: 500;
  }

  button{
    cursor: pointer;
  }

  @media (min-width: 700px){
    :root{
      font-size: 62.5%;
    }
  }

`;

export const Label = styled.label``;
