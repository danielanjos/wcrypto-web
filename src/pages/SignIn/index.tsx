import React from 'react';

import { Form } from '@unform/web';

import { Container } from './styles';
import Button from '../../components/Button';
import Input from '../../components/Input';

const SignIn: React.FC = () => {
  return (
    <Container>
      <h1>SignIn</h1>

      <Form onSubmit={() => console.log('singInSubmit')}>
        <Input name="input" />
        <Button type="button">Button</Button>
      </Form>
    </Container>
  );
};

export default SignIn;
